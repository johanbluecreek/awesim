
use crate::{
    misc::{NoiseType, NOISE_SCALE},
    background::{Background},
};

use bevy::prelude::{Vec2, Plugin, App};

use noise::{NoiseFn, Seedable};

/*
    Resources
*/

pub struct Noise {
    water: NoiseType,
    fields: NoiseType,
}

impl Noise {
    fn new() -> Self {
        Self {
            water: NoiseType::default().set_seed(256),
            fields: NoiseType::default().set_seed(42),
        }
    }
    pub fn get_background_type(&self, pos: &Vec2) -> Background {
        let water = self.water.get([(pos.x/NOISE_SCALE) as f64, (pos.y/NOISE_SCALE) as f64]) as f32;
        let fields = self.fields.get([(pos.x/NOISE_SCALE) as f64, (pos.y/NOISE_SCALE) as f64]) as f32;
        if water > 0.5 {
            return Background::Water
        } else if fields > 0.5 {
            return Background::Field
        }
        return Background::Grass
    }
}

/*
    Plugins
*/

pub struct NoisePlugin;

impl Plugin for NoisePlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Noise::new());
    }
}
