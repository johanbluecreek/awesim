
use::bevy::prelude::{Vec2, Vec3};
use noise::SuperSimplex;

/*
    Aliases
*/

pub type NoiseType = SuperSimplex;
pub type Point = (i32, i32);

/*
    Constants
*/

pub const GRID_SIZE: Vec2 = Vec2::from_array([10., 10.]);
const BUFFER: i32 = 4;
// 1920x1080/GRID_SIZE/2 + BUFFER:
pub const MAX_X: i32 = 96 + BUFFER;
pub const MAX_Y: i32 = 54 + BUFFER;
pub const NOISE_SCALE: f32 = 40.0;

/*
    Utility functions
*/

pub fn point_to_translation(pos: &Vec2) -> Vec3 {
    (pos.clone()*GRID_SIZE).extend(0.0)
}

pub fn translation_to_point(trans: &Vec3) -> Point {
    let new_trans = trans.truncate()/GRID_SIZE;
    return (new_trans.x as i32, new_trans.y as i32)
}
