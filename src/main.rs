
use awesim::{
    background::BackgroundPlugin,
    camera::CameraPlugin,
    noise::NoisePlugin,
};

use bevy::prelude::*;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(CameraPlugin)
        .add_plugin(BackgroundPlugin)
        .add_plugin(NoisePlugin)
        .run();
}
