
use bevy::prelude::*;

/*
    Resources
*/

pub struct ViewChange(pub bool);

/*
    Systems
*/

fn setup_camera(mut commands: Commands) {
    commands.spawn_bundle(Camera2dBundle::new_with_far(1000.0));
}

fn camera_panning_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut view_change: ResMut<ViewChange>,
    mut camera_query: Query<&mut Transform, With<OrthographicProjection>>,
) {
    let mut camera_transform = camera_query.single_mut();
    if keyboard_input.pressed(KeyCode::Right) ||
        keyboard_input.pressed(KeyCode::D) {
        camera_transform.translation.x += 10.0;
        view_change.0 = true;
    }
    if keyboard_input.pressed(KeyCode::Left) ||
        keyboard_input.pressed(KeyCode::A)  {
        camera_transform.translation.x -= 10.0;
        view_change.0 = true;
    }
    if keyboard_input.pressed(KeyCode::Up) ||
        keyboard_input.pressed(KeyCode::W)  {
        camera_transform.translation.y += 10.0;
        view_change.0 = true;
    }
    if keyboard_input.pressed(KeyCode::Down) ||
        keyboard_input.pressed(KeyCode::S)  {
        camera_transform.translation.y -= 10.0;
        view_change.0 = true;
    }
    if keyboard_input.pressed(KeyCode::R) {
        camera_transform.translation.x = 0.0;
        camera_transform.translation.y = 0.0;
        view_change.0 = true;
    }
}

/*
    Plugins
*/

pub struct CameraPlugin;

impl Plugin for CameraPlugin {
    fn build(&self, app: &mut App) {
        app
            .insert_resource(ViewChange(false))
            .add_startup_system(setup_camera)
            .add_system_set(
                SystemSet::new()
                    .with_system(camera_panning_system)
            );
    }
}
