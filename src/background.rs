
use crate::{
    camera::ViewChange,
    misc::{
        Point,
        point_to_translation, translation_to_point,
        MAX_X, MAX_Y, GRID_SIZE
    },
    noise::Noise,
};

use bevy::prelude::*;

use itertools::iproduct;
use std::collections::HashSet;

/*
    Components
*/

#[derive(Component)]
struct BackgroundBlock;

/*
    Resources
*/

struct CurrentBlocks(HashSet<Point>);

impl CurrentBlocks {
    fn new() -> Self {
        let mut set = HashSet::new();
        for a in iproduct!(-MAX_X..=MAX_X, -MAX_Y..=MAX_Y) {
            set.insert(a);
        }
        return Self(set)
    }
    fn update(&mut self, focus: &Point) {
        let mut set = HashSet::new();
        for a in iproduct!(-MAX_X..=MAX_X, -MAX_Y..=MAX_Y) {
            set.insert((a.0+focus.0, a.1+focus.1));
        }
        self.0 = set
    }
}

struct SpawnedBlocks(HashSet<Point>);

impl SpawnedBlocks {
    fn new() -> Self {
        SpawnedBlocks(HashSet::<Point>::new())
    }
}

/*
    Enums
*/

pub enum Background {
    Water,
    Field,
    Grass,
}

impl Background {
    fn get_sprite(&self) -> Sprite {
        return match *self {
            Background::Water => Sprite {
                    color: Color::rgb(0.0, 0.0, 0.7),
                    ..default()
                },
            Background::Field => Sprite {
                    color: Color::rgb(0.5, 0.5, 0.0),
                    ..default()
                },
            Background::Grass => Sprite {
                    color: Color::rgb(0.0, 0.4, 0.0),
                    ..default()
                },
        }
    }
}

/*
    Systems
*/

fn spawn_background(
    mut commands: Commands,
    noise: Res<Noise>,
    current_blocks: Res<CurrentBlocks>,
    mut spawned_blocks: ResMut<SpawnedBlocks>,
) {
    for point in current_blocks.0.difference(&spawned_blocks.0.clone()) {
        let pos = Vec2::new(point.0 as f32, point.1 as f32);
        commands.spawn()
            .insert_bundle(SpriteBundle {
                sprite: noise
                    .get_background_type(&pos)
                    .get_sprite(),
                transform: Transform {
                    translation: point_to_translation(&pos),
                    scale: GRID_SIZE.extend(1.0),
                    ..default()
                },
                ..default()
            })
            .insert(BackgroundBlock);
        spawned_blocks.0.insert(*point);
    }
}

fn despawn_background(
    mut commands: Commands,
    background_query: Query<(Entity, &Transform), With<BackgroundBlock>>,
    current_blocks: Res<CurrentBlocks>,
    mut spawned_blocks: ResMut<SpawnedBlocks>,
) {
    for (entity, transform) in background_query.iter() {
        let point = translation_to_point(&transform.translation);
        if !current_blocks.0.contains(&point) {
            commands.entity(entity).despawn();
            spawned_blocks.0.remove(&point);
        }
    }
}

fn update_current_blocks(
    mut current_blocks: ResMut<CurrentBlocks>,
    mut view_change: ResMut<ViewChange>,
    camera_query: Query<&mut Transform, With<OrthographicProjection>>,
) {
    if !view_change.0 {
        return ()
    }
    let camera_transform = camera_query.single();
    current_blocks.update(&translation_to_point(&camera_transform.translation));
    view_change.0 = false;
}

/*
    Plugins
*/

pub struct BackgroundPlugin;

impl Plugin for BackgroundPlugin {
    fn build(&self, app: &mut App) {
        app
            .insert_resource(CurrentBlocks::new())
            .insert_resource(SpawnedBlocks::new())
            .add_system_set(
                SystemSet::new()
                    .with_system(spawn_background)
                    .with_system(despawn_background)
                    .with_system(update_current_blocks)
            );
    }
}
